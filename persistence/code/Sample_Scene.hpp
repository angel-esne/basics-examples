/*
 * SAMPLE SCENE
 * Copyright © 2020+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <vector>
#include <basics/Audio_Player>
#include <basics/Canvas>
#include <basics/Scene>
#include <basics/Vector>

namespace example
{

    using    std::vector;
    using basics::Vector2f;
    using basics::Vector3f;

    class Sample_Scene : public basics::Scene
    {
    private:

        struct Circle
        {
            Vector2f position;
            float    radius;
            Vector3f color;             ///< Color (r, g, b) con valores en el intervalo 0-1

            /** Retorna true si el punto que tiene coordenadas (x, y) se encuentra dentro del círculo.
              * Para ello se comprueba si el cuadrado de la distancia del punto al centro del círculo
              * es menor que el cuadrado del radio.
              */
            bool contains (float x, float y) const
            {
                float  delta_x = position[0] - x;
                float  delta_y = position[1] - y;
                return delta_x * delta_x + delta_y * delta_y < radius * radius;
            }
        };

        bool             suspended;                         ///< true cuando la aplicación está en segundo plano
        unsigned         canvas_width;                      ///< Resolución virtual del display
        unsigned         canvas_height;

        vector< Circle > circles;                           ///< Array de círculos

        Circle         * touched_circle;                    ///< Puntero al círculo que se empezó a tocar (nullptr si no se toca ninguno)
        basics::Vector2f touch_delta;                       ///< Separación punto del círculo que se tocó a su centro

    public:

        Sample_Scene();

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override;
        void finalize   () override;
        void suspend    () override;
        void resume     () override;

        void handle (basics::Event & event) override;
        void render (basics::Graphics_Context::Accessor & context) override;

    private:

        void load_status ();
        void save_status ();

    };

}
