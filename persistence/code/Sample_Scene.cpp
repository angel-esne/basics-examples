/*
 * SAMPLE SCENE
 * Copyright © 2020+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Sample_Scene.hpp"
#include <basics/Application>
#include <basics/Canvas>
#include <basics/Display>
#include <basics/Log>
#include <basics/Window>
#include <fstream>

using namespace basics;
using namespace std;

namespace example
{

    Sample_Scene::Sample_Scene()
    :
        circles
        {
            { {  200, 200 }, 75, { 1, 0, 0 } },
            { {  200, 520 }, 75, { 0, 1, 0 } },
            { { 1080, 520 }, 75, { 0, 0, 1 } },
        }
    {
        canvas_width  = 1280;
        canvas_height =  720;
    }

    bool Sample_Scene::initialize ()
    {
        suspended      = false;
        touched_circle = nullptr;

        display.set_prevent_sleep (true);

        // El método initialize() será invocado justo antes de que la escena pase a ser visible, por lo
        // que puede ser un buen momento para cargar el estado que pueda haberse guardado anteriormente:

        load_status();

        return true;
    }

    void Sample_Scene::finalize ()
    {
        display.set_prevent_sleep (false);

        // El método finalize() será invocado justo antes de que la escena deje de ser visible (ya sea
        // porque se cambia a otra escena o porque se cierra el juego), por lo que puede ser un buen
        // momento para guardar el estado:

        save_status();
    }

    void Sample_Scene::suspend ()
    {
        suspended = true;

        display.set_prevent_sleep (false);
    }

    void Sample_Scene::resume ()
    {
        suspended = false;

        display.set_prevent_sleep (true);
    }

    void Sample_Scene::handle (basics::Event & event)
    {
        if (!suspended) switch (event.id)
        {
            case ID(touch-started):                 // El usuario toca la pantalla
            {
                float x = *event[ID(x) ].as< var::Float > ();
                float y = *event[ID(y) ].as< var::Float > ();

                for (auto circle = circles.rbegin (); circle != circles.rend (); ++circle)
                {
                    if (circle->contains (x, y))
                    {
                        touched_circle = &*circle;      // Dirección de objeto apuntado por iterador
                        touch_delta    =   circle->position - Vector2f{ x, y };
                        break;
                    }
                }

                break;
            }

            case ID(touch-moved):
            {
                if (touched_circle)
                {
                    touched_circle->position[0]  = *event[ID(x) ].as< var::Float > () + touch_delta[0];
                    touched_circle->position[1]  = *event[ID(y) ].as< var::Float > () + touch_delta[1];
                }

                break;
            }

            case ID(touch-ended):
            {
                touched_circle = nullptr;
                break;
            }
        }
    }

    void Sample_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear ();

                for (auto & circle : circles)
                {
                    canvas->set_color   (circle.color[0], circle.color[1], circle.color[2]);
                    canvas->fill_circle (circle.position, circle.radius);
                }
            }
        }
    }

    void Sample_Scene::load_status ()
    {
        // Se establece la ruta en la que se debería encontrar el archivo que guarda el estado:

        string path = application.get_internal_data_path () + "/save.data";

        basics::log.d (string("LOAD_STATUS: trying to read the status from ") + path);

        // Se crea un objeto std::ifstream que permitirá leer el archivo en modo binario:

        ifstream reader(path, ifstream::binary);

        if (reader)             // Si es false el archivo no se pudo abrir (posiblemente porque no existe)
        {
            // Se lee la posición de cada círculo:
            // Se asume que hay datos guardados para cada uno de los círculos del vector y que se
            // guardaron en el mismo orden en el que se leen. Hay que tener esto presente para hacer
            // los ajustes correspondientes si cambian las premisas.

            for (auto & circle : circles)
            {
                Vector2f loaded_position;       // Objeto en el que se guardarán los bytes leídos

                // Se leen tantos bytes como ocupa el objeto en memoria sobreescriendo su contenido
                // byte a byte:

                reader.read ((char *)&loaded_position, sizeof(loaded_position));

                if (!reader.fail () && !reader.bad ())
                {
                    // Si la lectura se completó sin errores se guarda el dato leído:

                    circle.position = loaded_position;
                }
                else
                {
                    basics::log.e ("ERROR AT LOAD_STATUS: failed reading from save.data.");
                    break;
                }
            }

            basics::log.d ("LOAD_STATUS: data read successfully.");
        }
        else
            basics::log.w ("LOAD_STATUS: save.data not found.");
    }

    void Sample_Scene::save_status ()
    {
        // Se establece la ruta del archivo en el que se guardará el estado:

        string path = application.get_internal_data_path () + "/save.data";

        basics::log.d (string("SAVE_STATUS: creating file at ") + path);

        // Se crea un objeto std::ofstream que permitirá escribir en el archivo en modo binario:
        // Si el archivo ya existe se descarta su contenido (trunc).

        ofstream writer(path, ofstream::binary | ofstream::trunc);

        if (writer)             // Si es false no se pudo crear o sobrescribir el archivo
        {
            // Se guarda la posición de cada uno de los círculos:

            for (auto & circle : circles)
            {
                // Se escriben en el archivo tantos bytes como ocupa el objeto en memoria (se reinterpreta
                // su dirección como la dirección de un array de bytes (char)):

                writer.write ((char *)&circle.position, sizeof(circle.position));

                if (!writer.good ())
                {
                    basics::log.d ("SAVE_STATUS: failed writing to save.data.");
                    break;
                }
            }

            if (writer.good ())
            {
                basics::log.d ("SAVE_STATUS: data written successfully.");
            }
        }
        else
            basics::log.e ("SAVE_STATUS: failed to create the file save.data.");
    }

}
