/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <basics/Canvas>
#include <basics/Scene>

namespace example
{

    class Sample_Scene : public basics::Scene
    {
    private:

        bool     suspended;                         ///< true cuando la aplicación está en segundo plano
        unsigned canvas_width;                      ///< Resolución virtual del display
        unsigned canvas_height;
        float    square_size;
        float    half_size;

        float    x;
        float    y;
        float    speed;

    public:

        Sample_Scene()
        {
            canvas_width  = 1280;                   // Todavía no se puede calcular el aspect ratio, por lo que se establece
            canvas_height =  720;                   // un tamaño por defecto hasta poder calcular el tamaño final
            square_size   =   50;
            half_size     =   25;
            speed         =   10;
        }

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override
        {
            x = canvas_width  / 2.f;
            y = canvas_height / 2.f;

            suspended = false;

            return true;
        }

        void suspend () override;
        void resume () override;

        void update (float ) override;
        void render (basics::Graphics_Context::Accessor & context) override;

    };

}
