/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Sample_Scene.hpp"
#include <basics/Accelerometer>
#include <basics/Canvas>
#include <basics/Director>

using namespace basics;
using namespace std;

namespace example
{

    void Sample_Scene::suspend ()
    {
        suspended = true;

        Accelerometer * accelerometer = Accelerometer::get_instance ();

        if (accelerometer) accelerometer->switch_off ();
    }

    void Sample_Scene::resume ()
    {
        suspended = false;

        Accelerometer * accelerometer = Accelerometer::get_instance ();

        if (accelerometer) accelerometer->switch_on ();
    }

    void Sample_Scene::update (float time)
    {
        Accelerometer * accelerometer = Accelerometer::get_instance ();

        if (accelerometer)
        {
            const Accelerometer::State & acceleration = accelerometer->get_state ();

            float roll  = atan2f ( acceleration.y, acceleration.z) * 57.3f;
            float pitch = atan2f (-acceleration.x, sqrtf (acceleration.y * acceleration.y + acceleration.z * acceleration.z)) * 57.3f;

            x += roll  * speed * time;
            y += pitch * speed * time;

            if (x - half_size <  0.f) x = half_size; else
            if (x + half_size >= canvas_width) x = canvas_width - half_size;

            if (y - half_size <  0.f) y = half_size; else
            if (y + half_size >= canvas_height) y = canvas_height - half_size;
        }
    }

    void Sample_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear ();
                canvas->set_color (1, 1, 1);
                canvas->fill_rectangle({ x - half_size, y - half_size }, { square_size, square_size });
            }
        }
    }

}
