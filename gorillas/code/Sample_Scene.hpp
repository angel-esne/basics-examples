/*
 * SAMPLE SCENE
 * Copyright © 2021+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <map>
#include <memory>
#include <string>
#include <vector>
#include <basics/Atlas>
#include <basics/Canvas>
#include <basics/Scene>
#include <basics/Size>
#include <basics/Vector>
#include <basics/Timer>

namespace example
{

    using basics::Atlas;
    using basics::Canvas;
    using basics::Point2f;
    using basics::Size2f;
    using basics::Timer;
    using basics::Vector2f;
    using basics::Vector3f;

    using std::map;
    using std::shared_ptr;
    using std::string;
    using std::vector;

    class Sample_Scene : public basics::Scene
    {
    private:

        struct Box
        {
            Vector2f position;
            Size2f   size;
            Vector3f color;

            Box() : position{ 0, 0 }, size{ 0, 0 }, color{ 0, 0, 0 }
            {
            }

            Box(const Vector2f & position, const Size2f & size, const Vector3f & color)
            :
                position(position), size(size), color(color)
            {
            }

            basics::Point2f center () const
            {
                return position + Vector2f(size[0], size[1]) * 0.5f;
            }

            bool contains (const Vector2f & point) const
            {
                return
                    point[0] > position[0] &&
                    point[1] > position[1] &&
                    point[0] < position[0] + size[0] &&
                    point[1] < position[1] + size[1];
            }

            virtual void render (basics::Canvas & canvas) const
            {
                canvas.set_color (color[0], color[1], color[2]);
                canvas.fill_rectangle (position, size);
            }
        };

        struct Sprite : Box
        {
            const Atlas::Slice * slice;
            Vector2f speed;
            bool     visible;

            Sprite() : slice(nullptr), speed{ 0.f, 0.f }, visible(true)
            {
            }

            Sprite(const Vector2f & position, const Size2f & size, const Atlas::Slice & slice)
            :
                Box(position, size, { 1, 1, 1 }),
                slice(&slice),
                speed(0, 0),
                visible(true)
            {
            }

            virtual void update (float delta)
            {
                position += speed * delta;
            }

            void render (basics::Canvas & canvas) const override
            {
                if (slice && visible)
                {
                    canvas.fill_rectangle (position, size, slice, basics::BOTTOM | basics::LEFT);
                }
            }
        };

        struct Animated_Sprite : Sprite
        {
        public:

            typedef vector< const Atlas::Slice * > Animation;
            typedef map   < string, Animation    > Animation_Set;

        protected:

            static constexpr float keyframe_duration = 0.1f;

            Animation_Set animations;

            Animation         * active_animation;
            Animation::iterator active_keyframe;
            float               keyframe_elapsed;

        public:

            Animated_Sprite
            (
                const Vector2f & initial_position,
                const string & default_animation_id,
                const Animation_Set & given_animations
            ) :
                animations(given_animations)
            {
                position = initial_position;

                set_animation (default_animation_id);
            }

            void set_animation (const string & id)
            {
                active_animation = &animations[id];
                active_keyframe  =  active_animation->begin ();

                update (keyframe_elapsed = 0.f);
            }

            void update (float delta) override
            {
                Sprite::update (delta);

                if (active_animation)
                {
                    if ((keyframe_elapsed += delta) >= keyframe_duration)
                    {
                        if (++active_keyframe == active_animation->end ()) active_keyframe = active_animation->begin ();

                        keyframe_elapsed = 0.f;
                    }

                    slice = *active_keyframe;
                    size  = { slice->width, slice->height };
                }
            }

        };

        struct Explosion : Sprite
        {
            enum { GROWING, SHRINKING } status;

            Vector2f center;
            Size2f   max_size;

            Explosion(const Vector2f & position, const Size2f & size, const Atlas::Slice & slice)
            :
                Sprite(position, { 0, 0 }, slice),
                status(GROWING),
                center(position),
                max_size(size)
            {
            }

            void reset (const Vector2f & new_position)
            {
                center   = position = new_position;
                size     = { 0.f, 0.f };
                status   = GROWING;
                visible  = true;
            }

            virtual void update (float delta) override
            {
                if (status == GROWING)
                {
                    size[0] = (size[0] + max_size[0]) * 0.5f;
                    size[1] = (size[1] + max_size[1]) * 0.5f;

                    if (size[0] > max_size[0] - 1.f) status = SHRINKING;
                }
                else
                {
                    size[0] *= 0.5f;
                    size[1] *= 0.5f;

                    if (size[0] < 1.f) visible = false;
                }

                position[0] = center[0] - size[0] * 0.5f;
                position[1] = center[1] - size[1] * 0.5f;
            }
        };

        struct Building : public Box
        {
            static constexpr float window_width  =  20.f;
            static constexpr float window_height =  40.f;
            static constexpr float window_step_x =  55.f;
            static constexpr float window_step_y = 100.f;

            vector< Box > windows;

            Building(float left_x, float width, int min_height, int max_height);

            virtual void render (Canvas & canvas) const override
            {
                Box::render (canvas);

                for (auto & window : windows) window.render (canvas);
            }
        };

        enum Status
        {
            UNINITIALIZED,
            PLAYING,
            GAME_OVER
        };

        static constexpr unsigned number_of_buildings =    8;
        static constexpr unsigned number_of_colors    =    3;
        static constexpr float    banana_speed_scale  =    2.f;
        static constexpr float    gravity_force       = -100.f * banana_speed_scale;

        static const Vector3f predefined_colors[number_of_colors];

        shared_ptr< Atlas >            atlas;

        vector< Building >             buildings;
        vector< shared_ptr< Sprite > > holes;
        vector< shared_ptr< Animated_Sprite > > gorillas;
        shared_ptr< Sprite >           banana;
        shared_ptr< Explosion >        explosion;
        shared_ptr< Sprite >           sun;
        shared_ptr< Sprite >           game_over;

        vector< Box    * >             background_boxes;
        vector< Box    * >             foreground_boxes;
        vector< Sprite * >             updateable_sprites;

        Status   status;
        bool     suspended;                                 ///< true cuando la aplicación está en segundo plano

        unsigned canvas_width;                              ///< Resolución virtual del display
        unsigned canvas_height;

        Animated_Sprite * player_gorilla;
        Animated_Sprite * touched_gorilla;
        Vector2f touch_location;

        Timer    timer;

    public:

        Sample_Scene();

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override;
        void finalize   () override;
        void suspend    () override;
        void resume     () override;

        void update (float delta) override;
        void handle (basics::Event & event) override;
        void render (basics::Graphics_Context::Accessor & context) override;

    private:

        void rebuild_scene      ();
        void shoot_banana       ();
        void make_explosion     ();
        void make_hole          ();
        void player_lost_game   ();
        void draw_thick_segment (Canvas & canvas, const Vector2f & a, const Vector2f & b);

    };

}
