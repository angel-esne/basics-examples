/*
 * SAMPLE SCENE
 * Copyright © 2021+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Sample_Scene.hpp"
#include <basics/Application>
#include <basics/Director>
#include <basics/Canvas>
#include <basics/Display>
#include <basics/Log>
#include <basics/Window>
#include <fstream>
#include <cstdlib>
#include <ctime>

using namespace basics;
using namespace std;

namespace example
{

    constexpr unsigned Sample_Scene::number_of_buildings;
    constexpr unsigned Sample_Scene::number_of_colors;
    constexpr float    Sample_Scene::banana_speed_scale;
    constexpr float    Sample_Scene::gravity_force;

    constexpr float    Sample_Scene::Building::window_width;
    constexpr float    Sample_Scene::Building::window_height;
    constexpr float    Sample_Scene::Building::window_step_x;
    constexpr float    Sample_Scene::Building::window_step_y;

    const Vector3f Sample_Scene::predefined_colors[] =
    {
        { 0.5f, 1.0f, 1.0f },
        { 1.0f, 0.5f, 1.0f },
        { 0.5f, 0.5f, 0.5f },
    };

    Sample_Scene::Building::Building(float left_x, float width, int min_height, int max_height)
    {
        position    = { left_x, 0.f };
        size.width  = width;
        size.height = float(min_height + rand () % (max_height - min_height));
        color       = predefined_colors[ rand () % number_of_colors];

        for (float y = size.height - window_step_y; y > -window_step_y; y -= window_step_y)
        {
            for (float x = left_x + window_step_x; x < left_x + size.width - window_step_x; x += window_step_x)
            {
                windows.emplace_back
                (
                    Vector2f{ x, y },
                    Size2f{ window_width, window_height },
                    rand () % 2 == 0 ? Vector3f{ 0, 0, 0 } : Vector3f{ 1, 1, 1 }
                );
            }
        }
    }

    Sample_Scene::Sample_Scene()
    {
        canvas_width    = 1920;
        canvas_height   = 1080;
        suspended       = false;
        status          = UNINITIALIZED;
        touched_gorilla = nullptr;
    }

    bool Sample_Scene::initialize ()
    {
        suspended = false;
        touched_gorilla = nullptr;

        display.set_prevent_sleep (true);

        srand (time (nullptr));

        return true;
    }

    void Sample_Scene::finalize ()
    {
        display.set_prevent_sleep (false);
    }

    void Sample_Scene::suspend ()
    {
        suspended = true;

        display.set_prevent_sleep (false);
    }

    void Sample_Scene::resume ()
    {
        suspended = false;

        display.set_prevent_sleep (true);
    }

    void Sample_Scene::update (float delta)
    {
        if (!atlas)
        {
            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                float real_aspect_ratio = float( context->get_surface_width () ) / float( context->get_surface_height () );

                canvas_width = unsigned( float( canvas_height ) * real_aspect_ratio );

                atlas = make_shared< Atlas > ("scene-elements.sprites", context);

                rebuild_scene ();
            }
        }
        else
        {
            if (status == GAME_OVER && timer.get_elapsed_seconds () > 3)
            {
                rebuild_scene ();
            }

            for (auto & sprite : updateable_sprites)
            {
                if (sprite->visible) sprite->update (delta);
            }

            if (banana->visible)
            {
                if (banana->position[0] < -banana->size.width || banana->position[0] > float(canvas_width) + banana->size.width)
                {
                    player_gorilla->set_animation ("stand");
                    banana->visible = false;
                }
                else
                {
                    for (auto & gorilla : gorillas)
                    {
                        if (gorilla.get () != player_gorilla && gorilla->contains (banana->position))
                        {
                            player_gorilla->set_animation ("stand");
                            make_explosion   ();
                            player_lost_game ();
                            return;
                        }
                    }

                    for (auto & building : buildings)
                    {
                        if (building.contains (banana->position))
                        {
                            player_gorilla->set_animation ("stand");
                            make_explosion ();
                            make_hole ();
                            return;
                        }
                    }

                    banana->speed[1] += gravity_force * delta;
                }
            }
        }
    }

    void Sample_Scene::handle (basics::Event & event)
    {
        if (!suspended && status == PLAYING) switch (event.id)
        {
            case ID(touch-started):                 // El usuario toca la pantalla
            {
                if (not banana->visible)
                {
                    touch_location =
                    {
                        *event[ID(x)].as< var::Float > (),
                        *event[ID(y)].as< var::Float > ()
                    };

                    for (auto & gorilla : gorillas)
                    {
                        if (gorilla->contains (touch_location))
                        {
                            touched_gorilla = gorilla.get ();
                            break;
                        }
                    }
                }

                break;
            }

            case ID(touch-moved):
            {
                if (touched_gorilla)
                {
                    touch_location =
                    {
                        *event[ID(x)].as< var::Float > (),
                        *event[ID(y)].as< var::Float > ()
                    };
                }

                break;
            }

            case ID(touch-ended):
            {
                if (touched_gorilla)
                {
                    touch_location =
                    {
                        *event[ID(x)].as< var::Float > (),
                        *event[ID(y)].as< var::Float > ()
                    };

                    shoot_banana ();

                    touched_gorilla = nullptr;
                }

                break;
            }
        }
    }

    void Sample_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->set_clear_color (0.f, 10.f / 255.f, 178.f / 255.f);
                canvas->clear ();

                for (auto & box : background_boxes) box->render (*canvas);
                for (auto & box : foreground_boxes) box->render (*canvas);

                if (touched_gorilla)
                {
                    canvas->set_color  (1, 1, 1);
                    draw_thick_segment (*canvas, touched_gorilla->center (), touch_location);
                }
            }
        }
    }

    void Sample_Scene::rebuild_scene ()
    {
        background_boxes.clear ();
        foreground_boxes.clear ();
        updateable_sprites.clear ();
        buildings.clear ();
        holes.clear ();
        gorillas.clear ();
        banana.reset ();
        sun.reset ();

        float building_left_x     = 0.f;
        float building_width      = float(canvas_width / number_of_buildings);
        int   building_min_height = int(canvas_height) / 5;
        int   building_max_height = int(canvas_height) * 3 / 4;

        for (unsigned count = 0; count < number_of_buildings; ++count)
        {
            buildings.emplace_back (building_left_x, building_width, building_min_height, building_max_height);

            building_left_x += building_width + 1.f;
        }

        auto & building_left  = buildings[1];
        auto & building_right = buildings[number_of_buildings - 2];

        auto * gorilla_stand_slice       = atlas->get_slice (ID(gorilla-standing));
        auto * gorilla_throw_left_slice  = atlas->get_slice (ID(gorilla-arm-up-1));
        auto * gorilla_throw_right_slice = atlas->get_slice (ID(gorilla-arm-up-2));

        gorillas.emplace_back
        (
            new Animated_Sprite
            (
                { building_left.position[0] + building_width * 0.5f - gorilla_stand_slice->width * 0.5f, building_left.size[1] },
                "stand",
                 Animated_Sprite::Animation_Set
                {
                    { "stand", { gorilla_stand_slice } },
                    { "throw", { gorilla_throw_left_slice } },
                    { "happy", { gorilla_throw_left_slice, gorilla_throw_right_slice } },
                }
            )
        );

        gorillas.emplace_back
        (
            new Animated_Sprite
            (
                { building_right.position[0] + building_width * 0.5f - gorilla_stand_slice->width * 0.5f, building_right.size[1] },
                "stand",
                 Animated_Sprite::Animation_Set
                {
                    { "stand", { gorilla_stand_slice } },
                    { "throw", { gorilla_throw_right_slice } },
                    { "happy", { gorilla_throw_right_slice, gorilla_throw_left_slice } },
                }
            )
        );

        auto * banana_keyframe_0_slice = atlas->get_slice (ID(banana-0));
        auto * banana_keyframe_1_slice = atlas->get_slice (ID(banana-1));
        auto * banana_keyframe_2_slice = atlas->get_slice (ID(banana-2));
        auto * banana_keyframe_3_slice = atlas->get_slice (ID(banana-3));

        banana = make_shared< Animated_Sprite >
        (
            Vector2f{ 0.f, 0.f },
            "roll",
            Animated_Sprite::Animation_Set
            {
                { "roll", { banana_keyframe_0_slice, banana_keyframe_1_slice, banana_keyframe_2_slice, banana_keyframe_3_slice } }
            }
        );

        banana->visible = false;

        auto * sun_slice = atlas->get_slice (ID(sun));

        sun = make_shared< Sprite >
        (
            Vector2f{ (float(canvas_width) - sun_slice->width) * 0.5f, float(canvas_height * 4 / 5) },
            Size2f{ sun_slice->width, sun_slice->height },
            *sun_slice
        );

        auto * game_over_slice = atlas->get_slice (ID(game-over));

        game_over = make_shared< Sprite >
        (
            Vector2f{ (float(canvas_width) - game_over_slice->width) * 0.5f, (float(canvas_height) - game_over_slice->height) * 0.5f },
            Size2f{ game_over_slice->width, game_over_slice->height },
            *game_over_slice
        );

        game_over->visible = false;

        auto * explosion_slice = atlas->get_slice (ID(explosion));

        explosion = make_shared< Explosion >
        (
            Vector2f{ 0.f, 0.f },
            Size2f{ explosion_slice->width, explosion_slice->height },
            *explosion_slice
        );

        explosion->visible = false;

        for (auto & building : buildings)
        {
            background_boxes.push_back (&building);
        }

        for (auto & gorilla : gorillas )
        {
            foreground_boxes.push_back (gorilla.get ());
            updateable_sprites.push_back (gorilla.get ());
        }

        background_boxes.push_back (sun.get ());

        foreground_boxes.push_back (banana.get ());
        updateable_sprites.push_back (banana.get ());

        foreground_boxes.push_back (explosion.get ());
        updateable_sprites.push_back (explosion.get ());

        foreground_boxes.push_back (game_over.get ());

        status = PLAYING;
    }

    void Sample_Scene::shoot_banana ()
    {
        player_gorilla = touched_gorilla;

        player_gorilla->set_animation ("throw");

        Vector2f shoot_position = player_gorilla->center ();

        banana->position =  shoot_position;
        banana->speed    = (shoot_position - touch_location) * banana_speed_scale;
        banana->visible  =  true;
    }

    void Sample_Scene::make_explosion ()
    {
        explosion->reset (banana->position);

        banana->visible = false;
    }

    void Sample_Scene::make_hole ()
    {
        auto * hole_slice = atlas->get_slice (ID(hole));

        holes.emplace_back
        (
        new Sprite
            (
                banana->position - Vector2f{ hole_slice->width, hole_slice->height } * 0.5f,
                Size2f{ hole_slice->width, hole_slice->height },
                *hole_slice
            )
        );

        background_boxes.push_back (holes.back ().get ());
    }

    void Sample_Scene::player_lost_game ()
    {
        status = GAME_OVER;

        player_gorilla->set_animation ("happy");

        game_over->visible = true;

        timer.reset ();
    }

    void Sample_Scene::draw_thick_segment (Canvas & canvas, const Vector2f & a, const Vector2f & b)
    {
        canvas.draw_segment (a, b);
        canvas.draw_segment (a + Vector2f{ 1, 0 }, b + Vector2f{ 1, 0 });
        canvas.draw_segment (a - Vector2f{ 1, 0 }, b - Vector2f{ 1, 0 });
        canvas.draw_segment (a + Vector2f{ 0, 1 }, b + Vector2f{ 0, 1 });
        canvas.draw_segment (a - Vector2f{ 0, 1 }, b - Vector2f{ 0, 1 });
    }

}
