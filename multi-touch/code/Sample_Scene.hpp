/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <basics/Canvas>
#include <basics/Scene>

namespace example
{

    class Sample_Scene : public basics::Scene
    {

        union Color
        {
            uint8_t  components[4];
            uint32_t value;

            Color(uint8_t r, uint8_t g, uint8_t b)
            {
                components[0] = r;
                components[1] = g;
                components[2] = b;
            }
        };

        struct Brush
        {
            bool  free;
            Color color;
        };

        struct Touch
        {
            int32_t id;                             ///< Un valor de ID negativo implica que el touch no está siendo usado
            float   x;
            float   y;
            Brush * brush;
        };

    private:

        bool     suspended;                         ///< true cuando la aplicación está en segundo plano
        unsigned canvas_width;                      ///< Resolución virtual del display
        unsigned canvas_height;
        float    square_size;
        float    half_size;

        Touch    touches[10];                       ///< Se gestionan hasta 10 touches simultáneos
        Brush    brushes[10];                       ///< Se dispone de 10 brochas, una para cada touch activo

    public:

        Sample_Scene();

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override;

        void suspend () override
        {
            suspended = true;
        }

        void resume () override
        {
            suspended = false;
        }

        void handle (basics::Event & event) override;
        void render (basics::Graphics_Context::Accessor & context) override;

    };

}
