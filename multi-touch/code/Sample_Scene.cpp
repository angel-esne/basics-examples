/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Sample_Scene.hpp"
#include <basics/Accelerometer>
#include <basics/Canvas>
#include <basics/Director>

using namespace basics;
using namespace std;

namespace example
{

    Sample_Scene::Sample_Scene()
    :
        brushes
        {
            { true, { 0xff,    0,    0 } },
            { true, {    0, 0xff,    0 } },
            { true, {    0,    0, 0xff } },
            { true, { 0xff, 0xff,    0 } },
            { true, { 0xff,    0, 0xff } },
            { true, { 0x80,    0,    0 } },
            { true, {    0, 0x80,    0 } },
            { true, {    0,    0, 0x80 } },
            { true, { 0x80, 0x80,    0 } },
            { true, { 0x80,    0, 0x80 } }
        }
    {
        canvas_width  = 1280;                   // Todavía no se puede calcular el aspect ratio, por lo que se establece
        canvas_height =  720;                   // un tamaño por defecto hasta poder calcular el tamaño final
        square_size   =  100;
        half_size     =   50;
    }

    bool Sample_Scene::initialize ()
    {
        suspended = false;

        for (auto & touch : touches) touch.id   = -1;
        for (auto & brush : brushes) brush.free = true;

        return true;
    }

    void Sample_Scene::handle (basics::Event & event)
    {
        if (!suspended) switch (event.id)
        {
            case ID(touch-started):                 // El usuario toca la pantalla
            {
                // Se busca un objeto touch disponible para ser vinculado al evento que se inicia:

                for (auto & touch : touches)
                {
                    if (touch.id < 0)
                    {
                        touch.id = *event[ID(id)].as< var::Int32 > ();
                        touch.x  = *event[ID(x) ].as< var::Float > ();
                        touch.y  = *event[ID(y) ].as< var::Float > ();

                        // Se busca una brocha libre para el touch:

                        for (auto & brush : brushes)
                        {
                            if (brush.free)
                            {
                                touch.brush = &brush;
                                brush.free  =  false;

                                break;
                            }
                        }

                        break;
                    }
                }

                break;
            }

            case ID(touch-moved):                   // El usuario arrastra el dedo/puntero por la pantalla
            {
                // Se busca el touch que se ha movido y se actualiza su posición:

                int32_t id = *event[ID(id)].as< var::Int32 > ();

                for (auto & touch : touches)
                {
                    if (touch.id == id)
                    {
                        touch.x = *event[ID(x)].as< var::Float > ();
                        touch.y = *event[ID(y)].as< var::Float > ();

                        break;
                    }
                }

                break;
            }

            case ID(touch-ended):                   // El usuario deja de tocar la pantalla
            {
                // Se busca el touch que se ha soltado y se desactiva:

                int32_t id = *event[ID(id)].as< var::Int32 > ();

                for (auto & touch : touches)
                {
                    if (touch.id == id)
                    {
                        touch.brush->free = true;
                        touch.id = -1;

                        break;
                    }
                }

                break;
            }
        }
    }

    void Sample_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear ();

                for (auto & touch : touches)
                {
                    if (touch.id >= 0)
                    {
                        Color & color = touch.brush->color;

                        float r = float(color.components[0]) * (1.f / 255.f);
                        float g = float(color.components[1]) * (1.f / 255.f);
                        float b = float(color.components[2]) * (1.f / 255.f);

                        canvas->set_color      (r, g, b);
                        canvas->fill_rectangle ({ touch.x - half_size, touch.y - half_size }, { square_size, square_size });
                    }
                }
            }
        }
    }

}
