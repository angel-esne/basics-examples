/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <basics/Canvas>
#include <basics/Scene>
#include <basics/Raster_Font>

namespace example
{

    class Sample_Scene : public basics::Scene
    {
    private:

        typedef std::unique_ptr< basics::Raster_Font > Font_Handle;

        bool        suspended;                      ///< true cuando la aplicación está en segundo plano
        unsigned    canvas_width;                   ///< Resolución virtual del display
        unsigned    canvas_height;

        Font_Handle font;

    public:

        Sample_Scene()
        {
            canvas_width  = 1280;                   // Todavía no se puede calcular el aspect ratio, por lo que se establece
            canvas_height =  720;                   // un tamaño por defecto hasta poder calcular el tamaño final
        }

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override
        {
            suspended = false;

            return true;
        }

        void suspend () override
        {
            suspended = true;
        }

        void resume () override
        {
            suspended = false;
        }

        void update (float ) override;
        void render (basics::Graphics_Context::Accessor & context) override;

    };

}
