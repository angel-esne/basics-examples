/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Sample_Scene.hpp"
#include <basics/Accelerometer>
#include <basics/Canvas>
#include <basics/Director>
#include <ctime>
#include <iomanip>
#include <sstream>

using namespace basics;
using namespace std;

namespace example
{

    void Sample_Scene::update (float time)
    {
        if (!font)
        {
            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                font.reset (new Raster_Font("fonts/impact.fnt", context));
            }
        }
    }

    void Sample_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear ();
                canvas->set_color (1, 1, 1);

                if (font)
                {
                    // Se dibujan textos con diferentes puntos de anclaje a partir de una cadena simple:

                    Text_Layout sample_text(*font, L"sample");

                    canvas->draw_text ({          0.f,           0.f }, sample_text, BOTTOM | LEFT );
                    canvas->draw_text ({          0.f, canvas_height }, sample_text,    TOP | LEFT );
                    canvas->draw_text ({ canvas_width,           0.f }, sample_text, BOTTOM | RIGHT);
                    canvas->draw_text ({ canvas_width, canvas_height }, sample_text,    TOP | RIGHT);

                    // Se guarda la fecha y la hora actual:

                    std::time_t time_point = std::time (nullptr);
                    std::tm   * time_data  = std::localtime (&time_point);

                    // Se convierte la fecha y la hora a una cadena de tipo wstring:

                    std::wostringstream buffer;

                    // Se convierte la fecha y se termina con un salto de línea:

                    buffer << time_data->tm_mday << "/" << (time_data->tm_mon + 1) << "/" << (time_data->tm_year + 1900) << "\n";

                    // Se convierte la hora asegurándose de que dada número tiene dos dígitos:

                    buffer << std::setfill (L'0');
                    buffer << std::setw (2) << time_data->tm_hour << ":";
                    buffer << std::setw (2) << time_data->tm_min  << ":";
                    buffer << std::setw (2) << time_data->tm_sec;

                    // Se crea el text layout a partir de la cadena creada:

                    Text_Layout time_text(*font, buffer.str ());

                    // Y se dibuja en el centro de la pantalla:

                    canvas->draw_text ({ canvas_width / 2.f, canvas_height / 2.f }, time_text, CENTER);
                }
            }
        }
    }

}
