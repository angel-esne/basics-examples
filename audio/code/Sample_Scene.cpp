/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Sample_Scene.hpp"
#include <basics/Accelerometer>
#include <basics/Canvas>
#include <basics/Director>
#include <basics/Audio_Player>

#include <basics/Log>

using namespace basics;
using namespace std;

namespace example
{

    Sample_Scene::Sample_Scene()
    {
        canvas_width  = 1280;                   // Todavía no se puede calcular el aspect ratio, por lo que se establece
        canvas_height =  720;                   // un tamaño por defecto hasta poder calcular el tamaño final
    }

    bool Sample_Scene::initialize ()
    {
        suspended = false;

        audio_playback_controller = audio_player.play ("music.ogg", true);

        return true;
    }

    void Sample_Scene::suspend ()
    {
        suspended = true;

        if (audio_playback_controller) audio_playback_controller->pause ();
    }

    void Sample_Scene::resume ()
    {
        suspended = false;

        if (audio_playback_controller) audio_playback_controller->play ();
    }

    void Sample_Scene::handle (basics::Event & event)
    {
        if (!suspended) switch (event.id)
        {
            case ID(touch-started):                 // El usuario toca la pantalla
            {
                // Se alterna el estado de la reproducción de audio cuando se toca la pantalla:

                if (audio_playback_controller)
                {
                    if (audio_playback_controller->get_status () == Audio_Player::Playback_Controller::PLAYING)
                    {
                        audio_playback_controller->pause ();
                    }
                    else
                        audio_playback_controller->play ();
                }
            }
        }
    }

    void Sample_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear ();
            }
        }
    }

}
