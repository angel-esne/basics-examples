/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <basics/Audio_Player>
#include <basics/Canvas>
#include <basics/Scene>

namespace example
{

    class Sample_Scene : public basics::Scene
    {
    private:

        bool     suspended;                         ///< true cuando la aplicación está en segundo plano
        unsigned canvas_width;                      ///< Resolución virtual del display
        unsigned canvas_height;

        basics::Audio_Player::Playback_Controller_Ptr audio_playback_controller;

    public:

        Sample_Scene();

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override;
        void suspend    () override;
        void resume     () override;

        void handle (basics::Event & event) override;
        void render (basics::Graphics_Context::Accessor & context) override;

    };

}
