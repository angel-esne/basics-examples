/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Sample_Scene.hpp"
#include <basics/Asset>
#include <basics/Log>
#include <string>
#include <sstream>

using namespace basics;
using namespace rapidxml;
using namespace std;

namespace example
{

    void Sample_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            // Se obtiene un canvas con el que poder dibujar:

            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                // Se borra la pantalla y se establece el color con el que se dibujarán las figuras:

                canvas->clear ();
                canvas->set_color (1, 1, 1);

                // Se recorre el array de figuras para dibujarlas una a una:

                for (auto & shape : shapes)
                {
                    // Se intenta convertir la figura actual a punto. Si la conversión no es posible
                    // el puntero point tendrá valor nullptr:

                    Point * point = dynamic_cast< Point * >(shape.get ());

                    if (point != nullptr)
                    {
                        canvas->draw_point(Point2f(point->position.x, point->position.y));

                        continue;   // Se pasa a la siguiente figura...
                    }
                }
            }
        }
    }

    void Sample_Scene::load_xml ()
    {
        // Se lee el contenido del archivo:

        auto xml_file = Asset::open ("shapes.xml");

        vector<byte> xml_bytes;

        xml_file->read_all (xml_bytes);

        // RapidXML espera un carácter nulo al final, por lo que hay que añadirlo:

        xml_bytes.push_back (0);

        // Se parsea el contenido del archivo XML:

        xml_document< > document;

        document.parse< 0 > ((char *)xml_bytes.data ());

        // Se obtiene el tag raíz (que debería ser <scene>) y se llama a la función que lo interpretará:

        xml_node< > * scene_node = document.first_node ();

        parse_scene_node (scene_node);
    }

    void Sample_Scene::parse_scene_node (rapidxml::xml_node< > * scene_node)
    {
        // Se recorre toda la lista de nodos anidados dentro de <scene> (solo el primer nivel):

        for (xml_node<> * child = scene_node->first_node (); child; child = child->next_sibling ())
        {
            if (child->type () == node_element)
            {
                string name(child->name ());

                if (name == "point")
                {
                    parse_point_node (child);
                }
                else if (name == "rectangle")
                {
                    parse_rectangle_node (child);
                }
            }
        }
    }

    void Sample_Scene::parse_point_node (xml_node< > * point_node)
    {
        Vector2f position;

        // Se recorren todos los tags anidados dentro de <point> guardando sus propiedades:

        for (xml_node<> * child = point_node->first_node (); child; child = child->next_sibling ())
        {
            if (child->type() == node_element)
            {
                if (string(child->name ()) == "position")
                {
                    position = parse_position_node (child);
                }
            }
        }

        // Una vez se conocen todas las propiedades de un punto, lo podemos crear y configurar:

        shared_ptr< Point > point(new Point);

        point->position.x = position[0];
        point->position.y = position[1];

        // Finalmente, el punto se guarda en el array de figuras geométricas:

        shapes.push_back (point);
    }

    void Sample_Scene::parse_rectangle_node (xml_node< > * rectangle_node)
    {
    }

    Vector2f Sample_Scene::parse_position_node (xml_node< > * position_node)
    {
        // Se asume que el primer nodo anidado dentro de <position> es un texto, por lo que se toma
        // directamente su valor:

        string position_text = position_node->first_node ()->value();

        // Se crea un istringstream para extraer texto formateado de la cadena. Se asume que serán
        // dos números separados por una coma:

        istringstream reader(position_text);

        // Se extraen los valores que esperamos encontrar:

        float x, y;
        char  comma;

        reader >> x;
        reader >> comma;
        reader >> y;

        // Se crea un vector geométrico con las dos coordenadas y se devuelve:

        return Vector2f(x, y);
    }

}
