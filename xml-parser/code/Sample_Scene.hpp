/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <vector>
#include <memory>
#include <rapidxml.hpp>
#include <basics/Canvas>
#include <basics/Scene>
#include <basics/Vector>

namespace example
{

    class Sample_Scene : public basics::Scene
    {
    private:

        // Modelo de datos para guardar los datos de las figuras geométricas:

        struct Shape
        {
            struct { float x, y; } position;
        protected:
            Shape() = default;
            virtual ~Shape() = default;
        };

        struct Point : Shape
        {
        };

        struct Rectangle : Shape
        {
            float width;
            float height;
        };

        /// Array de figuras geométricas que se llenará con los datos leídos del XML:

        std::vector< std::shared_ptr< Shape > > shapes;

        bool     suspended;                         ///< true cuando la aplicación está en segundo plano
        unsigned canvas_width;                      ///< Resolución virtual del display
        unsigned canvas_height;

    public:

        Sample_Scene()
        {
            canvas_width  = 1280;                   // Todavía no se puede calcular el aspect ratio, por lo que se establece
            canvas_height =  720;                   // un tamaño por defecto hasta poder calcular el tamaño final
        }

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        /** Este método se invoca automáticamente una vez cuando la escena pasa a ser visible.
         */
        bool initialize () override
        {
            suspended = false;

            load_xml ();

            return true;
        }

        void suspend () override
        {
            suspended = true;
        }

        void resume () override
        {
            suspended = false;
        }

        void render (basics::Graphics_Context::Accessor & context) override;

    private:

        /** Este método lee el archivo XML e interpreta su contenido.
         */
        void load_xml ();

        /** Este método parsea el tag <scene> que es la raíz del XML.
         */
        void parse_scene_node (rapidxml::xml_node< > * );

        /** Parsea cualquier tag <point> que se encuentre dentro de <scene>.
         */
        void parse_point_node (rapidxml::xml_node< > * );

        /** Parsea cualquier tag <rectangle> que se encuentre dentro de <scene>.
         */
        void parse_rectangle_node (rapidxml::xml_node< > * );

        /** Extrae y devuelve las coordenadas (X,Y) definidas dentro de un tag <position>.
         */
        basics::Vector2f parse_position_node (rapidxml::xml_node< > * );

    };

}
