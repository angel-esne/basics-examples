/*
 * SAMPLE SCENE
 * Copyright © 2020+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Sample_Scene.hpp"
#include <basics/Application>
#include <basics/Canvas>
#include <basics/Display>
#include <basics/Director>
#include <basics/Log>
#include <basics/Window>
#include <fstream>

using namespace basics;
using namespace std;

namespace example
{

    Sample_Scene::Sample_Scene()
    {
        status        = UNINITIALIZED;
        canvas_width  = 1280;
        canvas_height =  720;
    }

    bool Sample_Scene::initialize ()
    {
        suspended = false;

        display.set_prevent_sleep (true);

        return true;
    }

    void Sample_Scene::finalize ()
    {
        display.set_prevent_sleep (false);
    }

    void Sample_Scene::suspend ()
    {
        suspended = true;

        display.set_prevent_sleep (false);
    }

    void Sample_Scene::resume ()
    {
        suspended = false;

        display.set_prevent_sleep (true);
    }

    void Sample_Scene::handle (basics::Event & event)
    {
        if (!suspended && status == READY) switch (event.id)
        {
            case ID(touch-started):                 // El usuario toca la pantalla
            case ID(touch-moved):
            {
                float x = *event[ID(x) ].as< var::Float > ();
                float y = *event[ID(y) ].as< var::Float > ();

                for (auto square : squares)
                {
                    square->set_slice (square->contains ({ x, y }) ? ID(green-square) : ID(blue-square));
                }

                break;
            }

            case ID(touch-ended):
            {
                for (auto square : squares)
                {
                    square->set_slice (ID(blue-square));
                }

                break;
            }
        }
    }

    void Sample_Scene::update (float delta)
    {
        if (status == UNINITIALIZED)
        {
            if (load_images ()) create_sprites ();
        }
    }

    void Sample_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                if (status == ERROR)
                    canvas->set_clear_color (1, 0, 0);
                else
                    canvas->set_clear_color (1, 1, 1);

                canvas->clear ();

                for (auto & sprite : sprites)
                {
                    sprite->render (*canvas);
                }
            }
        }
    }

    bool Sample_Scene::load_images ()
    {
        Graphics_Context::Accessor context = director.lock_graphics_context ();

        if (context)
        {
            texture = Texture_2D::create (ID(texture), context, "background.png");

            if (texture)
            {
                context->add (texture);

                atlas = make_shared< Atlas > ("atlas.sprites", context);

                status = atlas->good () ? READY : ERROR;
            }
            else
                status = ERROR;
        }

        return status == READY;
    }

    void Sample_Scene::create_sprites ()
    {
        sprites.emplace_back (make_shared< Sprite > (texture.get ()));

        auto background = sprites.back ().get ();

        background->set_size   ({ canvas_width, canvas_height });
        background->set_anchor (BOTTOM | LEFT);

        sprites.emplace_back (make_shared< Sprite > (atlas.get (), ID(blue-square)));
        squares[0] = sprites.back ().get ();
        sprites.emplace_back (make_shared< Sprite > (atlas.get (), ID(blue-square)));
        squares[1] = sprites.back ().get ();
        sprites.emplace_back (make_shared< Sprite > (atlas.get (), ID(blue-square)));
        squares[2] = sprites.back ().get ();
        sprites.emplace_back (make_shared< Sprite > (atlas.get (), ID(blue-square)));
        squares[3] = sprites.back ().get ();

        Vector2f canvas_center{ canvas_width * .5f, canvas_height * .5f };

        squares[0]->set_position (canvas_center + Vector2f{ -20.f, +20.f });
        squares[0]->set_anchor   (BOTTOM | RIGHT);
        squares[0]->set_scale    (0.75f);
        squares[1]->set_position (canvas_center + Vector2f{ +20.f, +20.f });
        squares[1]->set_anchor   (BOTTOM | LEFT);
        squares[1]->set_scale    (1.00f);
        squares[2]->set_position (canvas_center + Vector2f{ -20.f, -20.f });
        squares[2]->set_anchor   (TOP | RIGHT);
        squares[2]->set_scale    (1.25f);
        squares[3]->set_position (canvas_center + Vector2f{ +20.f, -20.f });
        squares[3]->set_anchor   (TOP | LEFT);
        squares[3]->set_scale    (1.50f);
    }

}
