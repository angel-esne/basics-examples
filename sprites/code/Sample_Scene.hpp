/*
 * SAMPLE SCENE
 * Copyright © 2020+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <vector>
#include <basics/Canvas>
#include <basics/Scene>
#include <basics/Sprite>
#include <basics/Vector>

namespace example
{

    using    std::shared_ptr;
    using    std::vector;
    using basics::Atlas;
    using basics::Sprite;
    using basics::Texture_2D;

    class Sample_Scene : public basics::Scene
    {
    private:

        enum
        {
            UNINITIALIZED, READY, ERROR
        }
        status;

        bool     suspended;                       ///< true cuando la aplicación está en segundo plano
        unsigned canvas_width;                    ///< Resolución virtual del display
        unsigned canvas_height;

        shared_ptr< Texture_2D > texture;
        shared_ptr< Atlas      > atlas;

        vector< shared_ptr< Sprite > > sprites;

        Sprite * squares[4];

    public:

        Sample_Scene();

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override;
        void finalize   () override;
        void suspend    () override;
        void resume     () override;

        void handle (basics::Event & event) override;
        void update (float delta) override;
        void render (basics::Graphics_Context::Accessor & context) override;

    private:

        bool load_images    ();
        void create_sprites ();

    };

}
